﻿CREATE TABLE [dbo].[Countries] (
    [CountryId]      INT             IDENTITY (1, 1) NOT NULL,
    [CountryName]    NVARCHAR (MAX)  NOT NULL,
    [Capital]        NVARCHAR (MAX)  NOT NULL,
    [Population]     INT             NOT NULL,
    [Area]           INT             NOT NULL,
    [CommonLanguage] NVARCHAR (MAX)  NOT NULL,
    [Currency]       NVARCHAR (MAX)  NOT NULL,
    [Government]     NVARCHAR (MAX)  NOT NULL,
    [Continent]      NVARCHAR (MAX)  NOT NULL,
    [PhoneCode]      INT             NOT NULL,
    [TimeZone]       NVARCHAR (MAX)  NOT NULL,
    [FlagBinary]     VARBINARY (MAX) NULL,
    CONSTRAINT [PK_dbo.Countries] PRIMARY KEY CLUSTERED ([CountryId] ASC)
);





