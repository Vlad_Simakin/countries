﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using System.Threading.Tasks;

namespace CountriesLogic
{
    public class CountriesNetwork: IDisposable
    {
        CountriesEntities _db;

        public CountriesEntities db
        {
            get
            {
                return _db;
            }
        }

        public CountriesNetwork()
        {
            _db = new CountriesEntities();
        }

        public CountriesNetwork(CountriesEntities _Db)
        {
            _db = _Db;
        }

        public IQueryable<Countries> Countries()
        {
            return _db.Countries.OrderBy(o => o.CountryName);
        }

        public IQueryable<Countries> Find(string searchstring)
        {
            if(searchstring != null)
                return _db.Countries.Where(c => c.CountryName.Contains(searchstring)).OrderBy(c => c.CountryName);
            else
                return _db.Countries.OrderBy(o => o.CountryName);
        }

        public Countries Find(int id)
        {
            return _db.Countries.ToList<Countries>().Find(o => o.CountryId == id);
        }

        public void Edit(Countries country)
        {
            _db.Entry(country).State = System.Data.Entity.EntityState.Modified;
                _db.SaveChanges();
        }

        public void SaveNew(Countries country)
        {
            _db.Countries.Add(country);
            _db.SaveChanges();
        }

        public bool IsInDb(string CountryName)
        {
            return _db.Countries.Any(o => o.CountryName == CountryName);
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}