﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Data.Entity;
using System.Xml.Serialization;

namespace CountriesWebModel
{
    [XmlRoot(ElementName = "Countries")]
    public class CountriesModel
    {
        [Key]
        [HiddenInput(DisplayValue = false)]
        public int CountryId { get; set; }

        [Required]
        [Display(Name = "Страна")]
        public string CountryName { get; set; }

        [Required]
        [Display(Name = "Столица")]
        public string Capital { get; set; }

        [Required]
        [Display(Name = "Население")]
        public int? Population { get; set; }

        [Required]
        [Display(Name = "Площадь")]
        public int? Area { get; set; }

        [Required]
        [Display(Name = "Официальные языки")]
        public string CommonLanguage { get; set; }

        [Required]
        [Display(Name = "Валюта")]
        public string Currency { get; set; }

        [Required]
        [Display(Name = "Форма правления")]
        public string Government { get; set; }

        [Required]
        [Display(Name = "Континент")]
        public string Continent { get; set; }

        [Required]
        [Display(Name = "Телефонный код")]
        public int? PhoneCode { get; set; }

        [Required]
        [Display(Name = "Часовой пояс")]
        public string TimeZone { get; set; }

        [HiddenInput(DisplayValue = false)]
        public byte[] FlagBinary { get; set; }

        public static bool SupportMimeType(string mimeType)
        {
            switch (mimeType)
            {
                case ".jpg":
                case ".jpeg":
                case ".png":
                case ".gif":
                    return true;
            }
            return false;
        }
    }
}