﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using CountriesWebModel;
using System.IO;
using System.Xml.Serialization;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Loader
{
    public partial class Loader : ServiceBase
    {
        int eventId;
        DateTime scheduleTime;
        System.Timers.Timer timer;
        public Loader(string[] args)
        {
            scheduleTime = DateTime.Today.AddHours(Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["StartHour"]));
            if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains("Minutes"))
                scheduleTime = scheduleTime.AddMinutes(Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["Minutes"]));
            timer = new System.Timers.Timer();
            InitializeComponent();
            string eventSourceName = "Countries";
            string logName = "CountriesLog";
            if (args.Count() > 0)
            {
                eventSourceName = args[0];
            }
            if (args.Count() > 1)
            {
                logName = args[1];
            }
            eventLog1 = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists(eventSourceName))
            {
                System.Diagnostics.EventLog.CreateEventSource(eventSourceName, logName);
            } eventLog1.Source = eventSourceName; eventLog1.Log = logName;
        }

        protected override void OnStart(string[] args)
        {
            eventLog1.WriteEntry("In OnStart");
            if (scheduleTime.Subtract(DateTime.Now).TotalSeconds < 0)
                scheduleTime = scheduleTime.AddDays(1);
            timer.Interval = scheduleTime.Subtract(DateTime.Now).TotalSeconds * 1000;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        protected override void OnStop()
        {
            eventLog1.WriteEntry("In OnStop");
        }

        protected override void OnContinue()
        {
            eventLog1.WriteEntry("In OnContinue.");
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            ReadAndSave();

            //
            if (timer.Interval != 24 * 60 * 60 * 1000)
            {
                timer.Interval = 24 * 60 * 60 * 1000;
            }
        }

        public void ReadAndSave()
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings["webServicePath"]);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                XmlSerializer serializer = new XmlSerializer(typeof(CountriesModel));
                serializer.UnknownElement += new XmlElementEventHandler(this.FlagToBinary);

                string[] dirs = Directory.GetFiles(System.Configuration.ConfigurationManager.AppSettings["Path"], "*.xml");

                    foreach (var dir in dirs)
                    {
                        using (FileStream file = new FileStream(dir, FileMode.Open))
                        {
                            CountriesModel newCountry = (CountriesModel)serializer.Deserialize(file);

                            var response = client.GetAsync("api/countries/IsInDb/" + newCountry.CountryName).Result;
                            if (response.IsSuccessStatusCode && !response.Content.ReadAsAsync<bool>().Result)
                            {
                                client.PostAsJsonAsync("api/countries/Add", newCountry);
                                eventLog1.WriteEntry(newCountry.CountryName + " added to DB", EventLogEntryType.Information, eventId++);
                            }
                            else
                            {
                                eventLog1.WriteEntry(newCountry.CountryName + " is in Db", EventLogEntryType.Information, eventId++);
                            }
                        }
                    }
            }
            catch
            {
                eventLog1.WriteEntry("Error", EventLogEntryType.Information, eventId++);
            }
        }

        private void FlagToBinary(object sender, XmlElementEventArgs e)
        {
            Console.WriteLine(e.Element.InnerText);
            CountriesModel c = (CountriesModel)e.ObjectBeingDeserialized;
            using (BinaryReader reader = new BinaryReader(File.Open(e.Element.InnerText, FileMode.Open)))
            {
                c.FlagBinary = reader.ReadBytes((int)reader.BaseStream.Length);
            }
        }
    }
}
