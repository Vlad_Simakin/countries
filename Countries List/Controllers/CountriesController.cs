﻿using Countries_List.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using System.Web.Mvc;
using CountriesWebModel;
using PerpetuumSoft.Knockout;

namespace Countries_List.Controllers
{
    public class CountriesController : KnockoutController
    {
     //   private CountriesNetwork Countries = new CountriesNetwork();
        private HttpClient client = new HttpClient();

        public CountriesController()
        {
            client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings["webServicePath"]);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        [Authorize]
        public ActionResult CountriesList()
        {
            return View();
        }

        [Authorize]
        public ActionResult CountriesListPartial(string CurrentSearch, string searchstring, int? page, int pageSize = 3)
        {
            int currentPageIndex = page.HasValue ? page.Value : 1;
            if (pageSize > 0)
                ViewBag.PageSize = pageSize;
            else pageSize = ViewBag.PageSize == null ? ViewBag.PageSize : 3;
            if (searchstring != null)
            {
                page = 1;
            }
            else
            {
                
                searchstring = CurrentSearch;
            }
            ViewBag.CurrentSearch = searchstring;
            var response = client.GetAsync("api/countries/Find/" + searchstring).Result;

                if (response.IsSuccessStatusCode)
                {
                    return View(response.Content.ReadAsAsync<IEnumerable<CountriesModel>>().Result.ToPagedList<CountriesModel>(currentPageIndex, pageSize));
                }
                else
                    return HttpNotFound();

        }

        [Authorize]
        public ActionResult ViewCountry(int id)
        {       
            CountriesModel res;
            var response = client.GetAsync("api/countries/Info/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                res = response.Content.ReadAsAsync<CountriesModel>().Result;
                ViewBag.img = Convert.ToBase64String(res.FlagBinary);
                return View(res);
            }
            else
                return HttpNotFound();
        }

        [HttpPost]
        [Authorize]
        public bool EditCountry(CountriesModel newInfo, string flag)
        {
            if (ModelState.IsValid)
            {
                if (Request.Files.Count == 0)
                {
                    List<byte> binaryFlag = new List<byte>();
                    string[] elements = flag.Split(',');
                    foreach (var element in elements)
                    {
                        binaryFlag.Add(Byte.Parse(element));
                    }
                    newInfo.FlagBinary = binaryFlag.ToArray();
                }
                else
                {
                    ValidateFlag(newInfo, Request);
                }
                client.PostAsJsonAsync("api/countries/Edit", newInfo);
                   return true;
            }
            return false;
        }

        [HttpGet]
        [Authorize]
        public ActionResult AddCountry()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddCountry([Bind(Exclude = "CountryId")] CountriesModel newCountry)
        {
            var response = client.GetAsync("api/countries/IsInDb/" + newCountry.CountryName).Result;
            if (response.IsSuccessStatusCode && response.Content.ReadAsAsync<bool>().Result)
            {
                ModelState.AddModelError("", "Страна уже есть в списке");
                return View(newCountry);
            }
            else if (ModelState.IsValid)
            {
                if (ValidateFlag(newCountry, Request))
                {
                    client.PostAsJsonAsync("api/countries/Add", newCountry);
                    return RedirectToAction("CountriesList", "Countries");
                }
                else ModelState.AddModelError("", "Загрузите флаг");
            }
            return View(newCountry);
        }

        public bool ValidateFlag(CountriesModel newCountry, HttpRequestBase Request)
        {
            var file = Request.Files[0];
            var extension = Path.GetExtension(file.FileName);
            if (file.FileName != "")
            {
                if (!string.IsNullOrWhiteSpace(extension) && CountriesModel.SupportMimeType(extension))
                {
                  using (var binaryReader = new BinaryReader(file.InputStream))
                    {
                        newCountry.FlagBinary = binaryReader.ReadBytes(file.ContentLength);
                        return true;
                    }
                }
            }
            if (newCountry.FlagBinary == null)
                return false;
            else
                return true;
        }
    }
}
