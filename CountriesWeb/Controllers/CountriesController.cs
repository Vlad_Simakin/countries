﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using CountriesLogic;
using System.Web;
using CountriesWebModel;
using AutoMapper;

namespace WebApplication2.Controllers
{
    public class CountriesController : ApiController
    {
        CountriesNetwork Countries = new CountriesNetwork();

        // GET: api/Countries
        [Route("api/countries/Find/")]
        public IEnumerable<CountriesModel> GetCountries()
        {
            Mapper.CreateMap<CountriesLogic.Countries, CountriesModel>();
            return Mapper.Map<IEnumerable<CountriesLogic.Countries>, IEnumerable<CountriesModel>>(Countries.Countries());
        }

        [Route("api/countries/IsInDb/{countryName}")]
        public bool GetIsInDb(string CountryName)
        {
            return Countries.IsInDb(CountryName);
        }

        [Route("api/countries/Edit")]
        public void Edit(CountriesModel newCountry)
        {
            Mapper.CreateMap<CountriesModel, CountriesLogic.Countries>();
            Countries.Edit(Mapper.Map<CountriesModel, CountriesLogic.Countries>(newCountry));
        }

        [Route("api/countries/Add")]
        public void Add(CountriesModel newCountry)
        {
            Mapper.CreateMap<CountriesModel, CountriesLogic.Countries>();
            Countries.SaveNew(Mapper.Map<CountriesModel, CountriesLogic.Countries>(newCountry));
        }

        [Route("api/countries/Info/{id}")]
        public CountriesModel GetCountryById(int id)
        {
            Mapper.CreateMap<CountriesLogic.Countries, CountriesModel>();
            return Mapper.Map<CountriesLogic.Countries, CountriesModel>(Countries.Find(id));
        }

        [Route("api/countries/Find/{searchstring}")]
        public IEnumerable<CountriesModel> GetCountryByName(string searchstring)
        {
            Mapper.CreateMap<CountriesLogic.Countries, CountriesModel>();
            return Mapper.Map<IEnumerable<CountriesLogic.Countries>, IEnumerable<CountriesModel>>(Countries.Find(searchstring));
        }
    }
}
